$( document ).ready(function() {

	var arrayS = ['s1','s2','s3','s4','s5','s6','s7'];
	var arrayP = ['pop1','pop2','pop3','pop4','pop5','pop6','pop7'];
	var gallery = ['img/gallery/gallery02.jpg','img/gallery/gallery08.jpg',
								'img/gallery/gallery11.jpg','img/gallery/gallery13.jpg',
								'img/gallery/gallery14.jpg','img/gallery/gallery-new01.jpg',
								'img/gallery/gallery-new02.jpg','img/gallery/gallery-new03.jpg',
								'img/gallery/gallery-new07.jpg','img/gallery/gallery-new08.jpg',
								'img/gallery/gallery-new10.jpg','img/gallery/gallery-new12.jpg',
								'img/gallery/gallery-new13.jpg','img/gallery/gallery-new14.jpg',
								'img/gallery/gallery-new04.jpg','img/gallery/gallery-new05.jpg',
								'img/gallery/gallery15.jpg','img/gallery/gallery16.jpg'];


	for (var i = 0; i < arrayS.length; i++) {
		$('#'+arrayP[i]).css('display', 'none');
	}


	for (let i = 0; i < arrayS.length; i++) {
		$('.'+arrayS[i]).mouseover( function(){
			$('#'+arrayP[i]).fadeIn();
		});

		$('.'+arrayS[i]).mouseout(function() {
			$('#'+arrayP[i]).css('display', 'none');
		});
	}


	//Smooth scrolling

	$(function() {
		$('a[href*="#"]:not([href="#"])').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html, body').animate({
						scrollTop: target.offset().top
					}, 1000);
					return false;
				}
			}
		});
	});

	$('#gallery-wrapper').Kenburns({
			images: gallery,
			scale:0.9,
			duration:8000,
			fadeSpeed:800,
			ease3d:'ease-out',
			onSlideComplete: function(){
					console.log('slide ' + this.getSlideIndex());
			},
			onLoadingComplete: function(){
					console.log('image loading complete');
			}
	});

	// $('#gallery-wrapper-sm').Kenburns({
	// 		images: gallery,
	// 		scale:1,
	// 		duration:8000,
	// 		fadeSpeed:800,
	// 		ease3d:'ease-out',
	// 		onSlideComplete: function(){
	// 				console.log('slide ' + this.getSlideIndex());
	// 		},
	// 		onLoadingComplete: function(){
	// 				console.log('image loading complete');
	// 		}
	// });
	//




});


// Shrink Menu when scrolling down

$(window).scroll(function() {
	if ($(document).scrollTop() > 100) {
		$('nav').addClass('shrink');
	} else {
		$('nav').removeClass('shrink');
	}
});
