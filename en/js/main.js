$( document ).ready(function() {

	var arrayS = ['s1','s2','s3','s4','s5','s6','s7'];
	var arrayP = ['pop1','pop2','pop3','pop4','pop5','pop6','pop7'];

	for (var i = 0; i < arrayS.length; i++) {
		$('#'+arrayP[i]).css('display', 'none');
	}


	for (let i = 0; i < arrayS.length; i++) {
		$('.'+arrayS[i]).mouseover( function(){
			$('#'+arrayP[i]).fadeIn();
		});

		$('.'+arrayS[i]).mouseout(function() {
			$('#'+arrayP[i]).css('display', 'none');
		});
	}


	//Smooth scrolling

	$(function() {
		$('a[href*="#"]:not([href="#"])').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html, body').animate({
						scrollTop: target.offset().top
					}, 1000);
					return false;
				}
			}
		});
	});


});


// Shrink Menu when scrolling down

$(window).scroll(function() {
	if ($(document).scrollTop() > 100) {
		$('nav').addClass('shrink');
	} else {
		$('nav').removeClass('shrink');
	}
});
